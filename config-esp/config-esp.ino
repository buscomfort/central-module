// #define BLYNK_PRINT Serial
// #include <ESP8266_Lib.h>
// #include <BlynkSimpleShieldEsp8266.h>
// #include <SoftwareSerial.h>
// #define ESP8266_BAUD 115200

// char auth[] = "6bae2d0e7bfa4b6096feeaffa2030e4f";
// char ssid[] = "busGateway"; //You can replace the wifi name to your wifi

// char pass[] = "gateway0"; //Type password of your wifi.

// SoftwareSerial EspSerial(2, 3); // RX, TX
// ESP8266 wifi(&EspSerial);

// void setup()
// {
// 	Serial.begin(115200);
// 	EspSerial.begin(ESP8266_BAUD);
// 	Blynk.begin(auth, wifi, ssid, pass);
// }

// void loop()
// {
// 	// Serial.println("HEHE");
// 	// delay(1000);
// 	Blynk.run();
// 	// delay(10000);
// }

/*
 * uMQTTBroker demo for Arduino (C++-style)
 * 
 * The program defines a custom broker class with callbacks, 
 * starts it, subscribes locally to anything, and publishs a topic every second.
 * Try to connect from a remote client and publish something - the console will show this as well.
 */

#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"

/*
 * Your WiFi config here
 */
char ssid[] = "busGateway"; // your network SSID (name)
char pass[] = "gateway0";   // your network password
bool WiFiAP = false;		// Do yo want the ESP as AP?

/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker : public uMQTTBroker
{
public:
	virtual bool onConnect(IPAddress addr, uint16_t client_count)
	{
		Serial.println(addr.toString() + " connected");
		return true;
	}

	virtual bool onAuth(String username, String password)
	{
		Serial.println("Username/Password: " + username + "/" + password);
		return true;
	}

	virtual void onData(String topic, const char *data, uint32_t length)
	{
		char data_str[length + 1];
		os_memcpy(data_str, data, length);
		data_str[length] = '\0';

		Serial.println("received topic '" + topic + "' with data '" + (String)data_str + "'");
	}
};

myMQTTBroker myBroker;

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
	Serial.println("Connecting to " + (String)ssid);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, pass);

	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");

	Serial.println("WiFi connected");
	Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
	WiFi.mode(WIFI_AP);
	WiFi.softAP(ssid, pass);
	Serial.println("AP started");
	Serial.println("IP address: " + WiFi.softAPIP().toString());
}

void setup()
{
	Serial.begin(115200);
	Serial.println();
	Serial.println();

	// Start WiFi
	if (WiFiAP)
		startWiFiAP();
	else
		startWiFiClient();

	// Start the broker
	Serial.println("Starting MQTT broker");
	myBroker.init();

	/*
	 * Subscribe to anything
	 */
	myBroker.subscribe("#");
}

int counter = 0;

void loop()
{
	/*
	* Publish the counter value as String
	*/
	myBroker.publish("broker/counter", (String)counter++);

	// wait a second
	delay(1000);
}