# central-module

Embedded module to be used as the central receiver unity from the bus data, sent by the bus-module modules.e.

# Hardware

* ESP8286EX
* RadioEnge LoRaMESH
* EVAL-ADICUP3029
* Arduino Uno (Needed to configure the LoRaMESH)


# Requirements

* Arduino:
  * Arduino IDE >=1.8.10
  * SoftwareSerial >= 1.0.0
  * 