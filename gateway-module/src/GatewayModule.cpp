#include "GatewayModule.h"

char GatewayModule::ssid[40];
char GatewayModule::pass[40];

GatewayModule::GatewayModule()
{
}

void GatewayModule::Setup(char *ssid, char *pass)
{
	// Serial.begin(115200);
	// Serial.println();
	// Serial.println();

	// utilizando o pino 2 para ativar/desativar mensagens de DEBUG no MQTT
	pinMode(2, INPUT);

	strcpy(GatewayModule::ssid, "busGateway");
	strcpy(GatewayModule::pass, "gateway0");

	WiFiAP = false;

	// Start WiFi
	if (WiFiAP)
		StartWiFiAP();
	else
		StartWiFiClient();

	// Start the broker
	// Serial.println("Starting MQTT broker");
	mqttBroker.init();

	/*
	 * Subscribe to anything
	 */
	mqttBroker.subscribe("#");

	uint16_t deviceId = 0;
	SomeSerial *transparentSerial = new SomeSerial(&Serial);
	longRangeCommunicationModule.Setup(COMMMOD_RADIOENGELORAMESH, BUS_MODULE_PAYLOAD_SIZE, 5000, 2,
									   deviceId,
									   transparentSerial, LORAMESH_TRANS_BAUDRATE);
	longRangeCommunicationModule.SetupLogger(NULL);
}

void GatewayModule::ExecuteRoutine()
{
	// uint8_t *data = NULL;
	uint8_t data[MAX_BUFFER_SIZE];
	uint16_t data_length = 0;

	DebugMessage(String("S1"));
	if (ReceiveMessage(data, &data_length))
	{
		if (data != NULL && data_length > 0)
		{
			DebugMessage(String("S2"));
			SendMessage(data, data_length);
		}
	}

	// if (data != NULL)
	//     free(data);
}

String GatewayModule::ReceiveMessage()
{
	uint8_t *data = NULL;
	uint16_t data_length = 0;
	String message;
	ReceiveMessage(data, &data_length);

	for (int c = 0; c < data_length; c++)
	{
		message += String(data[c], HEX);
		message += ' ';
	}

	if (data != NULL)
		free(data);

	return message;
}

bool GatewayModule::ReceiveMessage(uint8_t *data, uint16_t *data_length)
{
	bool reply = false;
	DebugMessage(String(F("TRYING")));
	if (longRangeCommunicationModule.ReceiveData(5000, data, data_length) == true)
	{
		if (data == NULL)
			DebugMessage(String("DATA IS NULL"));
		if (data != NULL && data_length != NULL)
		{
			reply = longRangeCommunicationModule.SendAck(data, (*data_length));
			DebugMessage((String)(*data_length));
			DebugMessage((String)(longRangeCommunicationModule.radioEngeLoRaMESH->frame.size));
			DebugMessage((String)(longRangeCommunicationModule.computed_crc));
			Frame_Typedef *frme = &(longRangeCommunicationModule.radioEngeLoRaMESH->frame);
			DebugMessage(longRangeCommunicationModule.radioEngeLoRaMESH->frame.buffer,
						 longRangeCommunicationModule.radioEngeLoRaMESH->frame.size);
			if (reply == true)
				DebugMessage(String(F("ACK SENT")));
		}
		DebugMessage(String("M3"));
	}
	return reply;
}

void GatewayModule::SendMessage(uint8_t *data, uint16_t data_length)
{
	mqttBroker.publish("bus/data", data, data_length);
	delay(300);
}

void GatewayModule::SendMessage(String message)
{
	mqttBroker.publish("bus/data", message);
	delay(300);
}

void GatewayModule::DebugMessage(String message)
{
	if (digitalRead(2) == LOW)
		SendMessage(message);
}

void GatewayModule::DebugMessage(uint8_t *data, uint16_t data_length)
{
	if (digitalRead(2) == LOW)
		SendMessage(data, data_length);
}

/*
 * WiFi init stuff
 */
void GatewayModule::StartWiFiClient()
{
	// Serial.println("Connecting to " + (String)ssid);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, pass);

	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		// Serial.print(".");
	}
	// Serial.println("");

	// Serial.println("WiFi connected");
	// Serial.println("IP address: " + WiFi.localIP().toString());
}

void GatewayModule::StartWiFiAP()
{
	WiFi.mode(WIFI_AP);
	WiFi.softAP(ssid, pass);
	// Serial.println("AP started");
	// Serial.println("IP address: " + WiFi.softAPIP().toString());
}