#ifndef GATEWAYMODULE_H_
#define GATEWAYMODULE_H_

#include <Arduino.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <SomeSerial.h>
#include "CommunicationModule/CommunicationModule.h"
#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"

#define BUS_MODULE_PAYLOAD_SIZE 41
#define LORAMESH_TRANS_BAUDRATE 9600

typedef enum
{
	GATEWAYMOD_RADIOENGELORAMESH,
	GATEWAYMOD_ADICUP,
} GatewayModuleType;

/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker : public uMQTTBroker
{
public:
	virtual bool onConnect(IPAddress addr, uint16_t client_count)
	{
		// Serial.println(addr.toString() + " connected");
		return true;
	}

	virtual bool onAuth(String username, String password)
	{
		// Serial.println("Username/Password: " + username + "/" + password);
		return true;
	}

	virtual void onData(String topic, const char *data, uint32_t length)
	{
		char data_str[length + 1];
		os_memcpy(data_str, data, length);
		data_str[length] = '\0';

		// Serial.println("received topic '" + topic + "' with data '" + (String)data_str + "'");
	}
};

class GatewayModule
{
private:
	CommunicationModule longRangeCommunicationModule;
	// CommunicationModule shortRangeCommunicationModule;

	// uMQTTBroker mqttBroker;
	myMQTTBroker mqttBroker;

	/*
	 * Your WiFi config here
	 */
	static char ssid[40]; // your network SSID (name)
	static char pass[40]; // your network password
	bool WiFiAP;		  // Do yo want the ESP as AP?
public:
	GatewayModule();
	void Setup(char *ssid, char *pass);
	void ExecuteRoutine();
	String ReceiveMessage();
	bool ReceiveMessage(uint8_t *data, uint16_t *data_length);
	void SendMessage(uint8_t *data, uint16_t data_length);
	void SendMessage(String message);
	void DebugMessage(uint8_t *data, uint16_t data_length);
	void DebugMessage(String message);
	void StartWiFiAP();
	void StartWiFiClient();
};

#endif /* GATEWAYMODULE_H_ */