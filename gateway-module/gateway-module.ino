#include "src/GatewayModule.h"

// arduino.json lines
// "sketch": "gateway-module/gateway-module.ino",
// "board": "esp8266:esp8266:generic",

GatewayModule *gatewayModule = new GatewayModule();

char ssid[] = "busGateway";
char pass[] = "gateway0";

void setup()
{
	gatewayModule->Setup(ssid, pass);
}

int counter = 0;

void loop()
{
	/*
	 * Publish the counter value as String
	 */
	// myBroker.publish("broker/counter", (String)counter++);
	gatewayModule->DebugMessage("BE");
	gatewayModule->ExecuteRoutine();
	gatewayModule->DebugMessage("AE");
	// wait a second
	// delay(1000);
}