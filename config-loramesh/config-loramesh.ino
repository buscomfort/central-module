// LoRaMESH_Radioenge - Version: Latest
#include "src/LoRaMESH/LoRaMESH.h"
// #include "src/LoRaMESH/LoRaMESHMenu.h"
#include "src/LoRaMESH/LoRaMESHCli.h"

// arduino.json lines
// "sketch": "config-loramesh/config-loramesh.ino",
//  "board": "arduino:avr:uno",

/**
 * 
  Application example using the EndDevice LoRaMESH Radioenge together with
  the Arduino Uno.
  
  This script:
    - Initializes the software serial interface on the pins 6 (RX) and 7 (TX).
    - Reads the local device ID to check if it is a master or slave.
    - If it is a master:
      - Waits for messages from slaves and shows the payload on the monitor.
      - Replies each message with an acknowledge.
    - If it is a slave:
      - Configures the GPIO 5 as analog input.
      - Reads the analog inputs and sends to the master periodically.
*/

// LoRaMESH Configuration
#define LORAMESH_COMM_BAUDRATE 9600
#define LORAMESH_TX_COMM_PIN 4 // Pinos somente para o Arduino UNO, Serial2 é utilizada no Arduino Due
#define LORAMESH_RX_COMM_PIN 5 // Pinos da interface de comando

#define LORAMESH_TRANS_BAUDRATE 9600
#define LORAMESH_TX_TRANS_PIN 6 // Pinos somente para o Arduino UNO, Serial3 é utilizada no Arduino Due
#define LORAMESH_RX_TRANS_PIN 7 // Pinos da interface transparente

/* Includes ---------------------- */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <SoftwareSerial.h>
#include <SomeSerial.h>


/* Defines ----------------------- */
#define CMD_ANALOG 50
#define BUS_MODULE_N_DATA 13
#define BUS_MODULE_PAYLOAD_SIZE 25
#define BUS_MODULE_PAYLOAD_SIZE_WITH_INDEX BUS_MODULE_N_DATA + BUS_MODULE_PAYLOAD_SIZE
/* Payload buffer */
uint8_t bufferPayload[MAX_PAYLOAD_SIZE] = {0};
// uint8_t busModulePayload[BUS_MODULE_PAYLOAD_SIZE] = {0};

uint8_t payloadSize = 0;

/* Local device ID */
uint16_t localId;

/* Remote device ID */
uint16_t remoteId;

/* Received command */
uint8_t command;

/* SomeSerial handles */
SomeSerial *hSerialCommands = NULL;
SomeSerial *hSerialTransp = NULL;

HardwareSerial *logger = &Serial;

LoRaMESH localLM;

/* Initialization routine */
void setup()
{
	delay(500);
	logger->begin(115200);
	logger->print(F("STARTING\n\n"));

	/* Inicializando Seriais (Software ou Hardware) das interfaces da LoRaMESH */
	hSerialCommands = localLM.InitCommandSerial(LORAMESH_TX_COMM_PIN, LORAMESH_RX_COMM_PIN, LORAMESH_COMM_BAUDRATE);
	hSerialTransp = localLM.InitTransparentSerial(LORAMESH_TX_TRANS_PIN, LORAMESH_RX_TRANS_PIN, LORAMESH_TRANS_BAUDRATE);

	/* Gets the local device ID */
	// if (localLM.LocalRead(&localId, NULL) != MESH_OK)
	// 	logger->print(F("Couldn't read local ID\n\n"));
	// else
	// {
	// 	logger->println(F("LOCAL DEVICE INFO"));
	// 	logger->print(localLM.GetLocalDeviceInfoString());
	// 	if (localLM.ReadModParam(localLM.deviceInfo.id, NULL) != MESH_OK)
	// 		logger->print(F("Couldn't read modulation params\n\n"));
	// 	else
	// 	{
	// 		logger->println(F("MODULATION INFO"));
	// 		logger->print(localLM.GetLocalDeviceModInfoString());
	// 	}
	// 	logger->print("\n");
	// }

	// if (localId == 0) /* Is a master */
	// {
	// 	logger->print(F("Master\n"));
	// }

	// else /* Is a slave */
	// {
	// 	logger->print(F("Slave\n"));
	// 	delay(500);
	// }

	cli_init(logger, &localLM);
}
bool entrou = false;


	
/* Main loop */
void loop()
{
	// while (hSerialTransp->available())
	// 	logger->write(hSerialTransp->read());

	loramesh_cli();
	// if (logger && !entrou)
	// {
	// 	// mainMenu(logger, &localLM);
	// 	entrou = true;
	// }
	// logger->println(F("\nEND"));
	// while(!logger->available());

	// if (localLM.ReceivePacketTransp(&remoteId, bufferPayload, &payloadSize, 5000) != MESH_ERROR)
	// {
	// 	logger->println(F("REMOTE DEVICE INFO"));
	// 	logger->print(localLM.GetRemoteDeviceInfoString());
	// 	logger->print(F("Payload size: "));
	// 	logger->println(payloadSize);
	// 	for (int c = 0; c < payloadSize; c++)
	// 		logger->print(bufferPayload[c], HEX);
	// 	logger->print('\n');
	// }

	// delay(1000);
	// MeshStatus_Typedef preparationStatus;
	// uint8_t * currentPayload;
	// uint8_t * otherPayload;
	// uint16_t masterId = 0;
	// busSensorsData sData;

	// logger->println("STARTING ROUTINE");
	// sData.nmeaData = nmeaScanner->scanNmeaRMC("$GPRMC");
	// logger->println("GPS DONE");
	// busSensors->routine();
	// sData.temperature = busSensors->temperature;
	// sData.humidity = busSensors->humidity;
	// sData.meanNoiseRaw = busSensors->meanNoiseRaw;
	// logger->println("SENSORS DONE");

	// currentPayload = busSensors->convertToLoRaMESHPayload(sData);
	// // preparationStatus = PrepareFrameTranspBusModule(masterId, otherPayload, BUS_MODULE_PAYLOAD_SIZE_WITH_INDEX);
	// preparationStatus = PrepareFrameTranspBusModule(masterId, currentPayload, BUS_MODULE_PAYLOAD_SIZE);

	// if (preparationStatus == MESH_OK)
	//   SendPacket();
	// else
	//   logger->println("Mesh error");
	// logger->println("PACKAGE SENT");
	// delay(10000);

	// free(currentPayload);

	// if(GpioRead(localId, GPIO5, &AdcIn) != MESH_OK)
	//   logger->print("\nCommunication error\n\n");
	// else
	// {
	//   /* Sends to master */
	//   bufferPayload[0] = (uint8_t)AdcIn&0xFF;
	//   bufferPayload[1] = (uint8_t)(AdcIn>>8);

	//   PrepareFrameCommand(localId, CMD_ANALOG, bufferPayload, 2);  // Payload size = 2
	//   SendPacket();

	//   logger->println(AdcIn);

	//   /* Optional - Wait for the ACK message from the master */
	//   // ReceivePacketCommand(&remoteId, &command, bufferPayload, &payloadSize, 5000)
	// }
	// }
}
